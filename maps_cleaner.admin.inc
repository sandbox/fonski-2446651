<?php

/**
 * @file
 * The administration forms and configuration.
 */

use Drupal\maps_import\Cache\Object\Profile as CacheProfile;
use Drupal\maps_import\Profile\Profile;
use Drupal\maps_import\Converter\Converter;
use Drupal\maps_import\Mapping\MappingInterface;

/**
 * Menu callback; administrative overview.
 */
function maps_cleaner_admin_overview() {
  $rows = array();

  // List profiles.
  foreach (CacheProfile::getInstance()->loadAll() as $profile) {
    $row = array();

    $row[] = array(
      'data' => $profile->getTitle(),
    );

    $row[] = array(
      'data' => l(t('Configure'), 'admin/maps-suite/cleaner/manage/' . $profile->getPid()),
    );

    if ($profile->getFetchMethod() === 'ws') {
      $link = l(t('Webservice link'), "{$profile->getUrl()}?send=GET&privateToken={$profile->getToken()}&publicationId={$profile->getPublicationId()}&rootObjectId={$profile->getRootObjectId()}&templates={$profile->getWebTemplate()}&presetsGroup={$profile->getPresetGroupId()}&emptyCache={$profile->getOptionsItem('force_empty_cache')}");
    }

    $row[] = array(
      'data' => isset($link) ? $link : "",
    );

    $rows[] = $row;
  }

  return theme('table', array('rows' => $rows));
}

/**
 * The administration interface.
 *
 * @param Profile $profile
 *   The related profile.
 *
 * @return array
 *   The administration form.
 */
function maps_cleaner_manage_profile(Profile $profile) {
  // List converters.
  $rows = array();

  foreach (maps_import_get_converters($profile->getPid()) as $converter) {
    // Don't show child converters.
    if ($converter->getParentId() > 0) {
      continue;
    }

    $row = array();

    $row[] = array('data' => $converter->getTitle());

    $actions = array(
      l(t('Force re-import'), 'admin/maps-suite/cleaner/manage/' . $profile->getPid() . '/force-reimport/' . $converter->getCid()),
      l(t('Delete created entities'), 'admin/maps-suite/cleaner/manage/' . $profile->getPid() . '/delete-entities/' . $converter->getCid()),
      l(t('See created entities'), 'admin/maps-suite/cleaner/manage/' . $profile->getPid() . '/see-entities/' . $converter->getCid()),
    );

    foreach ($actions as $action) {
      $row[] = array('data' => $action);
    }

    $rows[] = $row;
  }

  return theme('table', array('rows' => $rows));
}

/**
 * Lists a converter created entities.
 *
 * @param Profile $profile
 *   The related profile.
 * @param Converter $converter
 *   The related converter.
 *
 * @return array
 *   The paginated list of entitites.
 */
function maps_cleaner_converter_see_entities(Profile $profile, Converter $converter) {
  $current_cid = (int) $converter->getCid();

  $cids = array();
  foreach (maps_import_get_converters($profile->getPid()) as $c) {
    if ($c->getParentId() === $current_cid) {
      $cids[] = (int) $c->getCid();
    }
  }

  $cids[] = $current_cid;

  // List entities.
  $rows = array();

  $query = db_select(MappingInterface::DB_ENTITIES_TABLE, 'e')
    ->extend('PagerDefault')
    ->limit(20);

  $query->fields('e')
    ->condition('cid', $cids, 'IN');

  $entities = $query->execute()
    ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

  foreach ($entities as $entity) {
    $e = entity_metadata_wrapper($entity['entity_type'], $entity['entity_id']);

    // Manage taxonomy term edit link.
    if ($entity['entity_type'] === 'taxonomy_term') {
      $term = taxonomy_term_load($entity['entity_id']);
      $uri = taxonomy_term_uri($term);

      $link = l(t('Go to entity'), $uri['path'] . '/edit');
    }
    // Manage any other entity edit link.
    else {
      $link = "";
      if (isset($e->edit_url)) {
        $link = l(t('Go to entity'), $e->edit_url->value());
      }
    }

    // Try catch to avoid error on taxonomy terms.
    try{
      $type = $e->type();
      $bundle = $e->getBundle();
    } catch(EntityMetadataWrapperException $e) {
      $type = $bundle = "";
    }

    $rows[] = array(
      'id' => $entity['entity_id'],
      'uid' => $entity['uid'],
      'cid' => $entity['cid'],
      'url' => $link,
      'type' => $type,
      'bundle' => $bundle,
      'import' => l(t('Force entity re-import'), 'admin/maps-suite/cleaner/manage/force-reimport-entity/' . $profile->getPid() . '/' . $entity['cid'] . '/' . $entity['uid']),
      'delete' => l(t('Delete entity'), 'admin/maps-suite/cleaner/manage/delete-entity/' . $entity['cid'] . '/' . $entity['entity_id']),
    );
  }

  $bundle = $converter->getBundle();
  $title = t('Entity') . ' : ' . $bundle;
  drupal_set_title($title);

  $header = array(
    'did' => 'Drupal ID',
    'uid' => 'MaPS UID',
    'cid' => 'Converter ID',
    'link' => 'Entity link',
    'type' => 'Entity type',
    'bundle' => 'Entity bundle',
    'reimport' => 'Force re-import',
    'delete' => 'Delete',
  );

  return theme('table', array('header' => $header, 'rows' => $rows)) . theme('pager');
}
