<?php

/**
 * @file
 * The drush commands file.
 */

use Drupal\maps_import\Cache\Object\Profile as CacheProfile;
use Drupal\maps_import\Cache\Object\Converter as CacheConverter;


/**
 * Implements hook_drush_command().
 */
function maps_cleaner_drush_command() {
  return array(
    'maps-cleaner' => array(
      'description' => 'Process the given operation.',
      'arguments' => array(
        'operation' => 'The operation to process',
      ),
      'options' => array(
        'pid' => 'The MaPS Import profile id.',
        'cid' => 'The MaPS Import converter id.',
        'uid' => 'The unique id used by MaPS Import.',
        'id' => 'The drupal id of a created entity MaPS Import.',
        'list-profiles' => 'List the available profiles',
        'list-converters' => 'List the available converters',
      ),
      'examples' => array(),
    ),
  );
}

/**
 * Process the command.
 */
function drush_maps_cleaner($operation = NULL) {
  $available_operations = array('force-reimport', 'delete-entities');

  if (drush_get_option('list-profiles')) {
    return drush_maps_cleaner_list_profiles();
  }

  if (drush_get_option('list-converters')) {
    return drush_maps_cleaner_list_converters();
  }

  if (is_null($operation) || !in_array($operation, $available_operations)) {
    echo "\nYou must specify an operation to process as first parameter.\nAvailable operations: " . implode(', ', $available_operations) . " \n";
    return FALSE;
  }

  if (!drush_get_option('pid') && !drush_get_option('cid')) {
    echo "You must specify a converter id or a profile id.\n";
    return FALSE;
  }

  switch ($operation) {
    case 'force-reimport':
      drush_maps_cleaner_force_reimport();
      break;

    case 'delete-entities':
      drush_maps_cleaner_delete_entities();
      break;
  }
}

/**
 * Process the force reimport operation.
 */
function drush_maps_cleaner_force_reimport() {
  $uid = drush_get_option('uid');
  $cid = drush_get_option('cid');

  if ($uid && $cid) {
    $converter = maps_import_converter_load($cid);
    return maps_cleaner_converter_force_reimport_process($converter->getProfile(), $converter, $uid);
  }

  if ($cid) {
    $converter = maps_import_converter_load($cid);
    return maps_cleaner_converter_force_reimport_process($converter->getProfile(), $converter);
  }

  if ($pid = drush_get_option('pid')) {
    $batch = maps_cleaner_profile_force_reimport_get_batch(maps_import_profile_load($pid));

    batch_set($batch);
    return drush_backend_batch_process();
  }
}

/**
 * Process the delete entities operation.
 */
function drush_maps_cleaner_delete_entities() {
  $id = drush_get_option('id');
  $cid = drush_get_option('cid');

  if ($id && $cid) {
    return maps_cleaner_converter_delete_entity(maps_import_converter_load($cid), $id);
  }

  if ($cid) {
    $batch = maps_cleaner_converter_delete_entities_get_batch(maps_import_converter_load($cid));
  }

  if ($pid = drush_get_option('pid')) {
    $batch = maps_cleaner_profile_delete_entities_get_batch(maps_import_profile_load($pid));
  }

  batch_set($batch);
  return drush_backend_batch_process();
}

/**
 * List the available profiles.
 */
function drush_maps_cleaner_list_profiles() {
  $mask = $mask = "| %5.5s | %20.20s | \n";
  $row_delimiter = str_pad('', 32, '-') . "\n";

  printf($row_delimiter);
  printf($mask, "Pid", "Title");
  printf($row_delimiter);

  foreach (CacheProfile::getInstance()->loadAll() as $profile) {
    printf($mask, $profile->getPid(), $profile->getTitle());
  }

  printf($row_delimiter);
}

/**
 * List the available converters.
 */
function drush_maps_cleaner_list_converters() {
  $mask = $mask = "| %5.5s | %20.20s | %5.5s | \n";
  $row_delimiter = str_pad('', 40, '-') . "\n";

  printf($row_delimiter);
  printf($mask, "Cid", "Title", "Pid");
  printf($row_delimiter);

  foreach (CacheConverter::getInstance()->loadAll() as $converter) {
    printf($mask, $converter->getCid(), $converter->getTitle(), $converter->getProfile()->getPid());
  }

  printf($row_delimiter);
}
